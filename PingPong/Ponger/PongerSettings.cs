﻿using RabbitMQ.Wrapper;

namespace Ponger
{
    class PongerSettings : MessangerSettings
    {
        public static string Message => "pong";
        public PongerSettings()
        {
            ConsumerQueue = "pong_queue";
            ConsumerKey = "pong_key";
            ProduserQueue = "ping_queue";
            ProduserKey = "ping_key";
        }
    }
}
