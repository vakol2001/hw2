﻿using RabbitMQ.Wrapper;
using System;
using System.Threading.Tasks;

namespace Ponger
{
    static class Program
    {
        public static void Main()
        {
            Task.Run(StartPonging);

            Console.WriteLine("Press [enter] to exit");
            Console.ReadLine();
        }

        private static async Task StartPonging()
        {
            var messager = new Messanger(new PongerSettings());

            while (true)
            {
                var message = await messager.ListenQueue();
                Console.WriteLine($"[{DateTime.Now.ToLongTimeString()}] {message}");
                await Task.Delay(2500);
                messager.SendMessageToQueue(PongerSettings.Message);

            }
        }
    }
}
