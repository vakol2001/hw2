﻿using RabbitMQ.Wrapper;

namespace Pinger
{
    class PingerSettings : MessangerSettings
    {
        public static string Message => "ping";
        public PingerSettings()
        {
            ConsumerQueue = "ping_queue";
            ConsumerKey = "ping_key";
            ProduserQueue = "pong_queue";
            ProduserKey = "pong_key";
        }
    }
}
