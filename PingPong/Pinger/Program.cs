﻿using RabbitMQ.Wrapper;
using System;
using System.Threading.Tasks;

namespace Pinger
{
    static class Program
    {
        public static void Main()
        {
            Task.Run(StartPinging);

            Console.WriteLine("Press [enter] to exit");
            Console.ReadLine();
        }

        private static async Task StartPinging()
        {
            var messager = new Messanger(new PingerSettings());

            while (true)
            {
                messager.SendMessageToQueue(PingerSettings.Message);
                var message = await messager.ListenQueue();
                Console.WriteLine($"[{DateTime.Now.ToLongTimeString()}] {message}");
                await Task.Delay(2500);

            }
        }
    }

}
