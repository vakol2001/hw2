﻿using RabbitMQ.Client;

namespace RabbitMQ.Wrapper
{
    static class PingPongSettings
    {
        public static string Uri => "amqp://guest:guest@localhost:5672";
        public static string ExchangeName => "PingPongExchange";
        public static string ExchangeType => RabbitMQ.Client.ExchangeType.Direct;
    }
}
