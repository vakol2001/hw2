﻿using RabbitMQ.Client;

namespace RabbitMQ.Wrapper.Models
{
    public class MessageConsumerSettings
    {
        public IModel Channel { get; set; }
        public bool SequentialFetch { get; set; }
        public string QueueName { get; set; }
        public bool AutoAcknowledge { get; set; }
    }
}
