﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.QueueServices
{
    public sealed class MessageQueue : IMessageQueue
    {
        readonly IConnection _connection;

        public IModel Channel { get; private set; }

        public MessageQueue(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory.CreateConnection();
            Channel = _connection.CreateModel();
        }

        public MessageQueue(IConnectionFactory connectionFactory, MessageScopeSettings settings) : this(connectionFactory)
        {
            DeclareExchange(settings.ExchangeName, settings.ExchangeType);

            if (settings.QueueName is not null)
            {
                BindQueue(settings.ExchangeName, settings.QueueName, settings.RoutingKey);
            }
        }

        private void BindQueue(string exchangeName, string queueName, string routingKey)
        {
            Channel.QueueDeclare(queueName, true, false, false);
            Channel.QueueBind(queueName, exchangeName, routingKey);
        }

        private void DeclareExchange(string exchangeName, string exchangeType)
        {
            Channel.ExchangeDeclare(exchangeName, exchangeType ?? string.Empty);
        }

        public void Dispose()
        {
            Channel?.Dispose();
            _connection?.Dispose();
        }
    }
}
