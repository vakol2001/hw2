﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.QueueServices
{
    public sealed class MessageProducerScope : IMessageProducerScope
    {
        readonly Lazy<IMessageQueue> _messageQueueLazy;
        readonly Lazy<IMessageProducer> _messageProducerLazy;

        readonly MessageScopeSettings _settings;
        readonly IConnectionFactory _connectionFactory;

        public MessageProducerScope(IConnectionFactory connectionFactory, MessageScopeSettings settings)
        {
            _settings = settings;
            _connectionFactory = connectionFactory;

            _messageProducerLazy = new Lazy<IMessageProducer>(CreateMessageProducer);
            _messageQueueLazy = new Lazy<IMessageQueue>(CreateMessageQueue);
        }

        public IMessageProducer MessageProducer => _messageProducerLazy.Value;
        private IMessageQueue MessageQueue => _messageQueueLazy.Value;
        private IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _settings);
        }

        private IMessageProducer CreateMessageProducer()
        {
            return new MessageProducer(new MessageProducerSettings
            {
                Channel = MessageQueue.Channel,
                PublicationAddress = new PublicationAddress(_settings.ExchangeType, _settings.ExchangeName, _settings.RoutingKey)
            });
        }

        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
    }
}
