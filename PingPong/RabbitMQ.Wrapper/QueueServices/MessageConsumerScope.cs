﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.QueueServices
{
    public sealed class MessageConsumerScope : IMessageConsumerScope
    {
        public IMessageConsumer MessageConsumer => _messageConsumerLazy.Value;
        private IMessageQueue MessageQueue => _messageQueueLazy.Value;

        readonly MessageScopeSettings _settings;
        readonly Lazy<IMessageQueue> _messageQueueLazy;
        readonly Lazy<IMessageConsumer> _messageConsumerLazy;

        readonly IConnectionFactory _connectionFactory;

        public MessageConsumerScope(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
        {
            _connectionFactory = connectionFactory;
            _settings = messageScopeSettings;

            _messageQueueLazy = new Lazy<IMessageQueue>(CreateMessageQueue);
            _messageConsumerLazy = new Lazy<IMessageConsumer>(CreateMessageConsumer);
        }

        private IMessageConsumer CreateMessageConsumer()
        {
            return new MessageConsumer(new MessageConsumerSettings
            {
                Channel = MessageQueue.Channel,
                QueueName = _settings.QueueName

            });
        }

        private IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _settings);
        }

        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
    }
}
