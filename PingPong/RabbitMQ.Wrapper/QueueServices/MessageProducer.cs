﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;
using System.Text;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageProducer : IMessageProducer
    {
        readonly MessageProducerSettings _settings;
        readonly IBasicProperties _properties;

        public MessageProducer(MessageProducerSettings settings)
        {
            _settings = settings;
            _properties = _settings.Channel.CreateBasicProperties();
            _properties.Persistent = true;

        }


        public void Send(string message, string type = null)
        {
            if (!string.IsNullOrEmpty(type))
            {
                _properties.Type = type;
            }

            var body = Encoding.UTF8.GetBytes(message);
            _settings.Channel.BasicPublish(_settings.PublicationAddress, _properties, body);
        }

        public void SendTyped(Type type, string message)
        {
            Send(message, type.AssemblyQualifiedName);
        }
    }
}
