﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageConsumerScopeFactory : IMessageConsumerScopeFactory
    {
        readonly IConnectionFactory _connectionFactory;

        public MessageConsumerScopeFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public IMessageConsumerScope Open(MessageScopeSettings settings)
        {
            return new MessageConsumerScope(_connectionFactory, settings);
        }

        public IMessageConsumerScope Connect(MessageScopeSettings settings)
        {
            var messageConsumerScope = Open(settings);
            messageConsumerScope.MessageConsumer.Connect();

            return messageConsumerScope;
        }
    }
}
