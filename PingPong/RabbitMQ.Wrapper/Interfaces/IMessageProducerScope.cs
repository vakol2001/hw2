﻿using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProducerScope : IDisposable
    {
        public IMessageProducer MessageProducer { get; }
    }
}
