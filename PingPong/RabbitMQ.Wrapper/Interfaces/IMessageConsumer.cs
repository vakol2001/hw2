﻿using RabbitMQ.Client.Events;
using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumer
    {
        event EventHandler<BasicDeliverEventArgs> Recived;
        void Connect();
        void SetAcknowledge(ulong deliveryTag, bool processed);
    }
}
