﻿using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumerScope : IDisposable
    {
        public IMessageConsumer MessageConsumer { get; }
    }
}
