﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.QueueServices;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper
{
    public class Messanger
    {
        private static IConnectionFactory _connectionFactory = new ConnectionFactory() { Uri = new Uri(PingPongSettings.Uri) };
        private static IMessageConsumerScopeFactory _consumerScopeFactory = new MessageConsumerScopeFactory(_connectionFactory);
        private static IMessageProducerScopeFactory _producerScopeFactory = new MessageProducerScopeFactory(_connectionFactory);

        private readonly IMessageProducerScope _producerScope;
        private readonly IMessageConsumerScope _consumerScope;


        private readonly SemaphoreSlim _semaphore = new(0, 1);
        private string _answer;
        public Messanger(MessangerSettings settings)
        {
            _producerScope = _producerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = PingPongSettings.ExchangeName,
                ExchangeType = PingPongSettings.ExchangeType,
                RoutingKey = settings.ProduserKey,
                QueueName = settings.ProduserQueue
            });

            _consumerScope = _consumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = PingPongSettings.ExchangeName,
                ExchangeType = PingPongSettings.ExchangeType,
                RoutingKey = settings.ConsumerKey,
                QueueName = settings.ConsumerQueue
            });

            _consumerScope.MessageConsumer.Recived += MessageConsumer_Recived;
        }

        private void MessageConsumer_Recived(object sender, BasicDeliverEventArgs args)
        {
            var body = args.Body.ToArray();
            _answer = Encoding.UTF8.GetString(body);
            _consumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, true);
            _semaphore.Release();
        }

        public async Task<string> ListenQueue()
        {
            await _semaphore.WaitAsync();
            return _answer;
        }
        public void SendMessageToQueue(string message)
        {
            _producerScope.MessageProducer.Send(message);
        }

    }
}
