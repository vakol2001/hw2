﻿namespace RabbitMQ.Wrapper
{
    public class MessangerSettings
    {
        public string ProduserQueue { get; set; }
        public string ProduserKey { get; set; }
        public string ConsumerQueue { get; set; }
        public string ConsumerKey { get; set; }
    }
}
